
#include <Servo.h> 

Servo bird1;  // Limits are 80 and 100 degrees
Servo bird2;  // Limits are 75 and 92 degrees
Servo bird3;  // Limits are 50 and 120 degrees
Servo bird4;  // Limits are 60 and 120 degrees

int pos = 0;    // variable to store the servo position 

void setup() 
{ 
bird1.attach(8);   // attaches the servo on pin 8 to Arduino
bird2.attach(9);   // attaches the servo on pin 9 to Arduino
bird3.attach(10);  // attaches the servo on pin 10 to Arduino
bird4.attach(11);  // attaches the servo on pin 11 to Arduino
} 


void loop() 
{                                   
bird1.write(90);
bird2.write(90);
bird3.write(90);
bird4.write(90);
delay(500);   
bird1.write(60);  
bird1.write(120);  
bird2.write(60);
bird1.write(60);
bird1.write(120);  
bird3.write(60);
bird1.write(60);
delay(500);  
bird4.write(60); 
bird1.write(120);
delay(500);              
bird1.write(60);  
delay(500);  
bird2.write(90);
bird1.write(120);
delay(500);  
bird3.write(90);
bird1.write(60);
delay(500);  
bird4.write(90);
bird1.write(120);
delay(500);  
bird1.write(63);
bird2.write(63);
bird3.write(63);
bird4.write(63);
delay(500);  
bird1.write(118);
bird2.write(118);
bird3.write(118);
bird4.write(118);
delay(500);  
bird1.write(63);
bird2.write(63);
bird3.write(63);
bird4.write(63);
bird1.write(118);
bird2.write(118);
bird3.write(118);
bird4.write(118);
delay(1000);
}
